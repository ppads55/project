from GeradorMapas import MapGenerator
import numpy as np


# Define elevation ranges and corresponding colors (test)
elevation_ranges = [0, 1, 500, 1000, 1500, 2000, 2500, 4000, 6000, 9000, 12000, 20000, 40000, np.inf]
colors = [
    (17, 54, 115),
    (85, 221, 68),
    (51, 187, 51),
    (0, 136, 34),
    (0, 85, 17),
    (0, 68, 17),
    (221, 153, 68),
    (187, 136, 51),
    (153, 102, 17),
    (136, 85, 17),
    (102, 68, 0),
    (68, 34, 0),
    (238, 238, 221),
    (238, 238, 221)
]

map = MapGenerator()
map.exportar('hgt', elevation_ranges, colors)
