
# Guia de Instalação de Dependências para o projeto

Este guia ajudará você a instalar as dependências necessárias para executar o script `main.py`, que requer as seguintes bibliotecas: NumPy, Pillow (PIL).

### Passo 1: Verificar se o Python está instalado

Antes de prosseguir, verifique se o Python está instalado no seu sistema. Abra um terminal ou prompt de comando e execute o seguinte comando:

```
python --version
```

Se o Python estiver instalado, você verá a versão instalada. Caso contrário, você precisará instalar o Python primeiro. Você pode baixá-lo em [python.org](https://www.python.org/downloads/).

### Passo 2: Instalar NumPy

NumPy é uma biblioteca fundamental para computação numérica em Python. Você pode instalá-lo usando pip, o gerenciador de pacotes do Python. Execute o seguinte comando no terminal ou prompt de comando:

```
pip install numpy
```

Isso instalará o NumPy e suas dependências.

### Passo 3: Instalar Pillow (PIL)

Pillow é uma biblioteca Python de processamento de imagem que é um fork do PIL (Python Imaging Library). Para instalá-lo, execute o seguinte comando:

```
pip install pillow
```

### Passo 4: Testar a aplicação

Depois de instalar as dependências, você pode verificar se foram instaladas corretamente executando o script `main.py`. Certifique-se de estar no diretório onde o `main.py` está localizado e execute o seguinte comando:

```
python main.py
```

Se não houver erros de importação, e o script rodar sem problemas, isso significa que as dependências foram instaladas corretamente.

Aqui está a seção adicionada ao guia de usuário:


# Arquivos de elevação SRTM de 90m
**Este repositório já possui os arquivos HGT de 90m da América do Sul**

## Requisitos de Entrada

Antes de executar o `main.py`, é importante garantir que você tenha os arquivos de elevação no formato HGT. Esses arquivos são necessários para processar e gerar visualizações de dados de elevação.

### Onde Obter os Arquivos de Elevação HGT

Os arquivos de elevação no formato HGT podem ser obtidos de várias fontes. Algumas opções incluem:

- **CloudCap Support**: O site [CloudCap Support](https://cloudcapsupport.com/elevation/World-90m/) oferece uma coleção de arquivos de elevação HGT para várias regiões do mundo.

- **NASA Earthdata Search**: O site [NASA Earthdata Search](https://search.earthdata.nasa.gov/search) fornece acesso a uma ampla variedade de dados de elevação e outras informações relacionadas à Terra.

- **JAXA Global ALOS 3D World**: A Agência Japonesa de Exploração Aeroespacial (JAXA) oferece dados de elevação global em alta resolução através do [Global ALOS 3D World](https://www.eorc.jaxa.jp/ALOS/en/aw3d30/index.htm).

- **Outros Provedores de Dados GIS**: Existem muitos outros provedores de dados GIS que oferecem arquivos de elevação HGT para diferentes regiões do mundo. Você pode pesquisar online ou verificar com organizações locais de mapeamento e geoespacial para obter acesso a esses dados.

Certifique-se de baixar os arquivos de elevação correspondentes à área de interesse antes de executar o `main.py`. Esses arquivos serão usados pelo script para gerar visualizações de dados de elevação.



## Utilizando o Software

Para utilizar o software e gerar visualizações de dados de elevação, siga os passos abaixo:

### 1. Baixar os Arquivos de Elevação HGT

Certifique-se de possuir os arquivos de elevação no formato HGT correspondentes à área de interesse. Você pode baixar esses arquivos de várias fontes, como o [CloudCap Support](https://cloudcapsupport.com/elevation/World-90m/), [NASA Earthdata Search](https://search.earthdata.nasa.gov/search), [JAXA Global ALOS 3D World](https://www.eorc.jaxa.jp/ALOS/en/aw3d30/index.htm) ou outros provedores de dados GIS, ou utilizar os dados da América do Sul fornecidos com o sistema.

### 2. Alterar as Tabelas de Elevação e Cores (Opcional)

Se desejar, você pode personalizar as tabelas de elevação e cores usadas para gerar as visualizações. No arquivo `main.py`, você encontrará as definições das tabelas de elevação (`elevation_ranges`) e cores correspondentes (`colors`). Você pode modificar essas listas conforme necessário para ajustar os intervalos de elevação e cores utilizadas nas visualizações.

O sistema vem implementado com o sistema de cores  default utilizados em navegação aérea, conforme apresentado abaixo:  


```python
# Define elevation ranges and corresponding colors
elevation_ranges = [0, 1, 500, 1000, 1500, 2000, 2500, 4000, 6000, 9000, 12000, 20000, 40000, np.inf]
colors = [
    (17, 54, 115),
    (85, 221, 68),
    (51, 187, 51),
    (0, 136, 34),
    (0, 85, 17),
    (0, 68, 17),
    (221, 153, 68),
    (187, 136, 51),
    (153, 102, 17),
    (136, 85, 17),
    (102, 68, 0),
    (68, 34, 0),
    (238, 238, 221),
    (238, 238, 221)
]
```

### 3. Executar o Arquivo main.py

Para gerar as visualizações de dados de elevação, execute o arquivo `main.py`. Você pode fazer isso abrindo um terminal ou prompt de comando, navegando até o diretório onde o arquivo `main.py` está localizado e executando o seguinte comando:

```
python main.py
```

Isso iniciará o processo de geração das visualizações. Aguarde até que o processo seja concluído e as visualizações sejam geradas com sucesso.

Após seguir esses passos, você terá gerado visualizações de dados de elevação com base nos arquivos HGT fornecidos e utilizando as tabelas de elevação e cores definidas no arquivo `main.py`.