import numpy as np
import os
from PIL import Image, ImageChops

class HGTManupilator:
    M_TO_FT = 3.28

    @staticmethod
    def parse_hgt_to_nparray(file_path, unit):
        with open(file_path, 'rb') as file:
            # Assuming SRTM1 HGT format (16-bit signed integers, big-endian)
            data = np.frombuffer(file.read(), dtype='>i2').reshape((1201, 1201)) * unit
        return data    

class MapGenerator:
    color_intensity = 160

    def __init__(self):
        pass

    def exportar(self, root_folder, elevation_ranges, colors):
        for foldername, _, filenames in os.walk(root_folder):
            for filename in filenames:
                if filename.endswith('.hgt'):
                    # Extract subfolder information
                    hgt_file_path = os.path.join(foldername, filename)
                    rel_path = os.path.relpath(hgt_file_path, root_folder)
                    subfolder = os.path.dirname(rel_path)

                    # create shaded relief (hills)
                    elevation_data = HGTManupilator.parse_hgt_to_nparray(hgt_file_path, HGTManupilator.M_TO_FT)
                    shaded_relief = self.create_shaded_relief_with_alpha(elevation_data)

                    # Output file paths in the "topo" folder
                    output_image_path = os.path.join('topo', subfolder, f'{os.path.splitext(filename)[0]}_hills.png')
                    self.save_image(shaded_relief, output_image_path, 'RGBA')

                    # create colorful map
                    colorful_elevation_map = self.create_colorful_elevation_map(elevation_data, elevation_ranges, colors)
                    output_image_path = os.path.join('topo', subfolder, f'{os.path.splitext(filename)[0]}_colors.png')
                    self.save_image(colorful_elevation_map, output_image_path, 'RGB')

                    # combine to get final map
                    background_image_path = os.path.join('topo', subfolder, f'{os.path.splitext(filename)[0]}_hills.png')
                    overlay_image_path = os.path.join('topo', subfolder, f'{os.path.splitext(filename)[0]}_colors.png')
                    output_image_path = os.path.join('topo', subfolder, f'{os.path.splitext(filename)[0]}.png')

                    self.overlay_images(background_image_path, overlay_image_path, output_image_path)

        
    @staticmethod
    def save_image(array, output_path, image_type):
        output_directory = os.path.dirname(output_path)
        os.makedirs(output_directory, exist_ok=True)
        Image.fromarray(array, image_type).save(output_path)

    @staticmethod
    def create_shaded_relief(elevation_data, azimuth, elevation):
        # Calculate gradient using simplified Sobel filter
        gradient_x = np.gradient(elevation_data, axis=1)
        gradient_y = np.gradient(elevation_data, axis=0)

        # Calculate hillshade intensity
        hillshade = (np.sin(np.radians(elevation)) * np.sin(np.radians(gradient_y))
                    + np.cos(np.radians(elevation)) * np.cos(np.radians(gradient_y))
                    * np.cos(np.radians(azimuth - 90 - gradient_x)))

        # Normalize to [0, 255] for image representation
        normalized_hillshade = (hillshade - hillshade.min()) / (hillshade.max() - hillshade.min())
        return normalized_hillshade

    @staticmethod
    def create_shaded_relief_with_alpha(elevation_data, azimuth=300, elevation=40):
        normalized_hillshade = MapGenerator.create_shaded_relief(elevation_data, azimuth, elevation)
        # Create RGBA array with alpha channel representing brightness
        shaded_relief_rgba = np.zeros((elevation_data.shape[0], elevation_data.shape[1], 4), dtype=np.uint8)
        shaded_relief_rgba[:, :, 0] = normalized_hillshade * 160  # Red channel
        shaded_relief_rgba[:, :, 1] = normalized_hillshade * 160  # Green channel
        shaded_relief_rgba[:, :, 2] = normalized_hillshade * 160  # Blue channel
        shaded_relief_rgba[:, :, 3] = (255-normalized_hillshade * 128).astype(np.uint8)  # Alpha channel
        return shaded_relief_rgba

    @staticmethod
    def create_colorful_elevation_map(elevation_data, elevation_ranges, colors):
        # Create an empty RGB image
        rgb_image = np.zeros((1201, 1201, 3), dtype=np.uint8)

        # Assign colors based on elevation ranges
        for i in range(len(elevation_ranges) - 1):
            mask = np.logical_and(elevation_data >= elevation_ranges[i], elevation_data < elevation_ranges[i + 1])
            rgb_image[mask] = colors[i]
        return rgb_image

    @staticmethod
    def overlay_images(background_path, overlay_path, output_path, alpha=0.5):
        # Open the background and overlay images
        background = Image.open(background_path)
        overlay = Image.open(overlay_path)

        # Resize overlay image to match the background size
        overlay = overlay.resize(background.size, Image.LANCZOS)

        # Prepare overlay image with alpha channel
        overlay = overlay.convert("RGBA")
        overlay_with_alpha = Image.new("RGBA", overlay.size)
        overlay_with_alpha = Image.blend(overlay_with_alpha, overlay, alpha)
        over_multiply = ImageChops.multiply(overlay, background)
        over_multiply = over_multiply.convert("RGB")
        over_multiply.save(output_path, "PNG")